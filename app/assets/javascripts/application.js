// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery.turbolinks
//= require jquery_ujs
//= require jquery.ui.all
//= require bootstrap-sprockets
//= require select2
//= require turbolinks
//= require jquery.dynatable
//= require_tree .

$(function() {
  return $("#dob_field").datepicker({
    changeMonth: true,
    changeYear: true,
    yearRange: '1950:2015',
    onSelect: function(date, inst) {
      var age, today;
      today = new Date();
      age = today.getFullYear() - inst.selectedYear;
      return $("#age_field").val(age);
    }
  });
});

