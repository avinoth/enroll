$ ->
  registers = $('#registers_table').dynatable(
    table:
      defaultColumnIdStyle: 'underscore'
    inputs:
      paginationClass: 'pagination pagination-sm'
    features:
      recordCount: false
     ).data('dynatable')

  registers_table = (registers_data) ->
    registers.settings.dataset.originalRecords = registers_data
    registers.process(true)

  url = $('#driver').data('remote')
  $.getJSON url, (resp) ->
    registers_table(resp)


