class CollegesController < ApplicationController
  before_action :set_college, only: [:show, :edit, :update, :destroy]

  def index
    @colleges = College.paginate(page: params[:page]).order(created_at: :desc)
  end

  def show
  end

  def new
    @college = College.new
  end

  def edit
  end

  def create
    @college = College.new(college_params)

    respond_to do |format|
      if @college.save
        format.html { redirect_to @college, notice: 'College was successfully created.' }
        format.json { render :show, status: :created, location: @college }
      else
        format.html { render :new }
        format.json { render json: @college.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @college.update(college_params)
        format.html { redirect_to @college, notice: 'College was successfully updated.' }
        format.json { render :show, status: :ok, location: @college }
      else
        format.html { render :edit }
        format.json { render json: @college.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @college.destroy
    respond_to do |format|
      format.html { redirect_to colleges_url, notice: 'College was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

    def set_college
      @college = College.find(params[:id])
    end


    def college_params
      params.require(:college).permit(:name, :country, :sat_min_score, :sat_max_score, :tuition_fees)
    end
end
