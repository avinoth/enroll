class RegistersController < ApplicationController

  def index
  end

  def all_registers
    @registers = Register.includes(:student, :college).all
    result = @registers.map do |register|
      {
        student_name: register.student.name,
        student_sat_score: register.student.sat_score,
        college_name: register.college.name,
        college_fees: register.college.tuition_fees,
        registered_on: register.created_at.strftime("%d-%m-%Y")
      }
    end
    render json: result
  end

end
