class StudentsController < ApplicationController
  before_action :set_student, only: [:show, :edit, :update, :destroy]

  def index
    @students = Student.all
  end

  def show
    @registered_colleges = @student.colleges
  end

  def new
    @student = Student.new
    @colleges = College.all
  end

  def edit
    @registered_colleges = @student.registers.pluck(:college_id)
    @colleges = College.eligible(@student.sat_score)
  end

  def create
    @student = Student.new(student_params)

    respond_to do |format|
      if @student.save
        create_registrations(@student, params[:collegeIds]) if params[:collegeIds].present?
        format.html { redirect_to @student, notice: 'Student was successfully created.' }
        format.json { render :show, status: :created, location: @student }
      else
        format.html { render :new }
        format.json { render json: @student.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    college_ids = params[:collegeIds].present? ? params[:collegeIds].map(&:to_i) : []
    respond_to do |format|
      if @student.update(student_params)
        update_registrations(@student, college_ids)
        format.html { redirect_to @student, notice: 'Student was successfully updated.' }
        format.json { render :show, status: :ok, location: @student }
      else
        format.html { render :edit }
        format.json { render json: @student.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @student.destroy
    respond_to do |format|
      format.html { redirect_to students_url, notice: 'Student was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_student
      @student = Student.find(params[:id])
    end

    def student_params
      params.require(:student).permit(:name, :sex, :age, :dob, :current_school, :current_level, :country, :sat_score)
    end

    def create_registrations student, colleges
      colleges.each do |college|
        student.registers.create(:college_id => college.to_i)
      end
    end

    def update_registrations student, colleges
      new_colleges =  colleges - student.registered_college_ids
      deleted_colleges = student.registered_college_ids - colleges
      create_registrations(student, new_colleges) if new_colleges.present?

      if deleted_colleges.present?
        deleted_colleges.each do |college|
          student.registers.where(:college_id => college).destroy_all
        end
      end
    end

end
