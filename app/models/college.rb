class College < ActiveRecord::Base
  has_many :registers, dependent: :destroy
  has_many :students, through: :registers

  scope :eligible, -> (sat_score) {where('sat_min_score <= ?', sat_score)}
end
