class Student < ActiveRecord::Base
  belongs_to :mentor

  has_many :registers, dependent: :destroy
  has_many :colleges, through: :registers

  def registered_college_ids
    self.colleges.pluck(:id).compact
  end

end
