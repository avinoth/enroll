json.array!(@mentors) do |mentor|
  json.extract! mentor, :id, :name, :sex, :age, :dob, :bio, :image
  json.url mentor_url(mentor, format: :json)
end
