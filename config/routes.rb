Rails.application.routes.draw do
  resources :colleges
  resources :mentors
  resources :students
  resources :registers, only: [:index]

  get '/all_registers', to: 'registers#all_registers', as: :all_registers

  root 'students#index'
end
