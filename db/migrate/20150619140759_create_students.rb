class CreateStudents < ActiveRecord::Migration
  def change
    create_table :students do |t|
      t.string :name, null: false
      t.string :sex
      t.integer :age
      t.date :dob
      t.string :current_school
      t.string :current_level
      t.string :country
      t.integer :sat_score, default: 0

      t.timestamps null: false
    end
  end
end
