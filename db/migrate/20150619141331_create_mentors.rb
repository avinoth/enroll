class CreateMentors < ActiveRecord::Migration
  def change
    create_table :mentors do |t|
      t.string :name, null: false
      t.string :sex
      t.integer :age
      t.date :dob
      t.string :bio
      t.string :image

      t.timestamps null: false
    end
  end
end
