class CreateColleges < ActiveRecord::Migration
  def change
    create_table :colleges do |t|
      t.string :name, null: false
      t.string :country
      t.integer :sat_min_score, default: 0
      t.integer :sat_max_score, default: 0
      t.integer :tuition_fees, default: 0

      t.timestamps null: false
    end
  end
end
