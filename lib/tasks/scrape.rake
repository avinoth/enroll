require 'open-uri'

namespace :scrape do
  task commonapp: [:environment] do
    url = "https://commonapp.org/ca4app/PublicPages/AllMembers"
    begin
      resp = HTTParty.get(url)
    rescue e
      puts "Error Fetching URL - #{url}"
    end
    list = JSON.parse(resp.body)

    page =  Nokogiri::HTML::Document.parse(list["ContentView"])

    page.css('li').each do |el|
      College.create(:name => el.text)
      puts "Created College - #{el.text}"
    end
  end

  task usnews: [:environment] do
    puts "Started Scraping USNews"
    base_url = "http://colleges.usnews.rankingsandreviews.com/best-colleges/rankings/national-universities/data"
    begin
      puts "Fetching URL - #{base_url}"
      page = Nokogiri::HTML(open(base_url))
    rescue e
      puts "Error Fetching URL - #{base_url}"
    end
    last_page = page.css('#pagination a')[-2].text.to_i

    (1..last_page).each do |pno|
      url = base_url + "/page+#{pno}"
      begin
        puts "Fetching URL - #{url}"
        doc = Nokogiri::HTML(open(url))
      rescue e
        puts "Error Fetching URL - #{url}"
      end

      doc.css(".ranking-data tbody tr").each do |row|
        name = row.css("td.college_name a").text
        tuition = tuition_fees row.css("td.search_tuition_display").text.strip
        college = College.find_or_create_by(:name => name)
        college.tuition_fees = tuition
        college.save
        puts "Updated College - #{name} with Tuition - #{tuition}"
      end
    end
    puts "Scraping Complete"
  end

  def tuition_fees val
    val = val.match(/\$\d+,\d+/)
    if val
      val[0].gsub(/[$,]/, "").to_i
    else
      0
    end
  end

end
